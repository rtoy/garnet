#!/bin/sh

#
# $Id$
#

#
# Configure these paths for your system.
#
SBCL=/usr/local/bin/sbcl
CMUCL=/usr/local/bin/lisp
ACL=/usr/local/allegro/8.1/alisp
CCL=/usr/local/bin/ccl

usage()
{
echo "
  Usage: `basename $0` <platform>

  where platform is one of:

	sbcl	-- Steel Bank Common Lisp ($SBCL)

	cmucl	-- CMU Common Lisp ($CMUCL)

	acl	-- Allegro Common Lisp (ANSI Version) ($ACL)

  (If the above paths aren't correct, they should be set at the beginning
  of the file $0).
"
}

if [ $# -ne 1 ]; then
    usage; exit 0;
fi

case $1 in
    sbcl)
	BUILD="$SBCL --disable-debugger"
	LOAD=$SBCL
	QUIT="(quit)"
	IMAGE=garnet-sbcl
	;;
    ccl)
	BUILD="$CCL --batch"
	LOAD=$CCL
	QUIT="(quit)"
	IMAGE=garnet-ccl
	;;
    cmucl)
	BUILD="$CMUCL -batch"
	LOAD=$CMUCL
	QUIT="(quit)"
	IMAGE=garnet-cmucl
	;;
    acl)
	BUILD="$ACL -batch -backtrace-on-error"
	LOAD=$ACL
	QUIT="(exit)"
	IMAGE=garnet.dxl
	;;
    *)
	usage;
	exit -1
	;;
esac


$BUILD << EOF
(load "garnet-prepare-compile")
(defvar Garnet-Garnet-Debug NIL) ;; no debugging code
(load "garnet-loader")
(load "garnet-compiler")
$QUIT
EOF

[ $? -eq 0 ] && $LOAD << EOF
(load "garnet-loader")

(let ((args (list "$IMAGE"
		  #+allegro :quit #+allegro t
                  #+sbcl :executable #+sbcl t
                  #+cmu :executable #+cmu t
                  #+ccl :prepend-kernel #+ccl t)))
 (apply #'opal:make-image args))
EOF
